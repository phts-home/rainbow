unit uFDlgAbout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ShellAPI;

type
  TFDlgAbout = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Bevel1: TBevel;
    Button1: TButton;
    procedure Label6Click(Sender: TObject);
    procedure Label7Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDlgAbout: TFDlgAbout;

implementation

uses uFMain;

{$R *.dfm}

procedure TFDlgAbout.Label6Click(Sender: TObject);
begin
  ShellExecute(0,'',pchar('mailto:'+Label6.Caption+'?subject=Rainbow'),'','',1);
end;

procedure TFDlgAbout.Label7Click(Sender: TObject);
begin
  ShellExecute(0,'',pchar(Label7.Caption),'','',1);
end;

procedure TFDlgAbout.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FMain.Timer1.Enabled:=not FMain.Winner;
end;

procedure TFDlgAbout.FormShow(Sender: TObject);
begin
  FMain.Timer1.Enabled:=False;
  Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
  Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
end;

end.
