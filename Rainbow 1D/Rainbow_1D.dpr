program Rainbow_1D;

uses
  Forms,
  uFMain in 'uFMain.pas' {FMain},
  uFDlgRes in 'uFDlgRes.pas' {FDlgRes},
  uFDlgName in 'uFDlgName.pas' {FDlgName},
  uFDlgHelp in 'uFDlgHelp.pas' {FDlgHelp},
  uFDlgAbout in 'uFDlgAbout.pas' {FDlgAbout};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := '������ 1D';
  Application.CreateForm(TFMain, FMain);
  Application.CreateForm(TFDlgRes, FDlgRes);
  Application.CreateForm(TFDlgName, FDlgName);
  Application.CreateForm(TFDlgHelp, FDlgHelp);
  Application.CreateForm(TFDlgAbout, FDlgAbout);
  Application.Run;
end.
