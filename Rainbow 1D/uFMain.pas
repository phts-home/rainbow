unit uFMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ImgList, Buttons, Grids, DBGrids, Menus,
  ShellAPI;

type
  TFMain = class(TForm)
    ImageList1: TImageList;
    Image1: TImage;
    SpeedButton01_1: TSpeedButton;
    SpeedButton02_2: TSpeedButton;
    SpeedButton03_3: TSpeedButton;
    SpeedButton10_3: TSpeedButton;
    SpeedButton11_2: TSpeedButton;
    SpeedButton12_1: TSpeedButton;
    SpeedButton13_4: TSpeedButton;
    SpeedButton14_5: TSpeedButton;
    SpeedButton16_4: TSpeedButton;
    SpeedButton17_5: TSpeedButton;
    SpeedButton31_1: TSpeedButton;
    SpeedButton32_2: TSpeedButton;
    SpeedButton33_3: TSpeedButton;
    SpeedButton34_4: TSpeedButton;
    SpeedButton35_5: TSpeedButton;
    SpeedButton36_1: TSpeedButton;
    SpeedButton37_2: TSpeedButton;
    SpeedButton38_3: TSpeedButton;
    SpeedButton39_4: TSpeedButton;
    SpeedButton40_5: TSpeedButton;
    SpeedButton41_1: TSpeedButton;
    SpeedButton42_2: TSpeedButton;
    SpeedButton43_3: TSpeedButton;
    SpeedButton44_4: TSpeedButton;
    SpeedButton45_4: TSpeedButton;
    SpeedButton46_3: TSpeedButton;
    SpeedButton47_2: TSpeedButton;
    SpeedButton48_1: TSpeedButton;
    SpeedButton01_6: TSpeedButton;
    SpeedButton02_1: TSpeedButton;
    SpeedButton03_2: TSpeedButton;
    SpeedButton04_3: TSpeedButton;
    SpeedButton05_4: TSpeedButton;
    SpeedButton06_5: TSpeedButton;
    SpeedButton07_6: TSpeedButton;
    SpeedButton02_6: TSpeedButton;
    SpeedButton03_1: TSpeedButton;
    SpeedButton04_2: TSpeedButton;
    SpeedButton05_3: TSpeedButton;
    SpeedButton06_4: TSpeedButton;
    SpeedButton07_5: TSpeedButton;
    SpeedButton08_6: TSpeedButton;
    SpeedButton51_1: TSpeedButton;
    SpeedButton51_2: TSpeedButton;
    SpeedButton51_3: TSpeedButton;
    SpeedButton51_4: TSpeedButton;
    SpeedButton51_5: TSpeedButton;
    SpeedButton51_6: TSpeedButton;
    SpeedButton51_7: TSpeedButton;
    SpeedButton52_6: TSpeedButton;
    SpeedButton52_5: TSpeedButton;
    SpeedButton52_4: TSpeedButton;
    SpeedButton52_3: TSpeedButton;
    SpeedButton52_1: TSpeedButton;
    SpeedButton52_2: TSpeedButton;
    SpeedButton52_7: TSpeedButton;
    SpeedButton01_7: TSpeedButton;
    SpeedButton02_7: TSpeedButton;
    ImageList2: TImageList;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N5: TMenuItem;
    N44: TMenuItem;
    N55: TMenuItem;
    N66: TMenuItem;
    N77: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N6: TMenuItem;
    N9: TMenuItem;
    N4: TMenuItem;
    N2: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N3: TMenuItem;
    Label_min: TLabel;
    Label_sep: TLabel;
    Label_sec: TLabel;
    Bevel1: TBevel;
    Timer1: TTimer;
    procedure SpeedButton01_1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton12_1Click(Sender: TObject);
    procedure SpeedButton09_1Click(Sender: TObject);
    procedure SpeedButton04_1Click(Sender: TObject);
    procedure N44Click(Sender: TObject);
    procedure N55Click(Sender: TObject);
    procedure N66Click(Sender: TObject);
    procedure N77Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
  private
    { Private declarations }
  public
    Bmp: TBitmap;
    a7: Array [1..7,1..7] of Integer;
    CurGame,LastGame: Byte;
    Winner: Boolean;
    procedure CheckPlace(GameType: Byte);
    procedure ChangeGlyph(Value: Byte);
    procedure HideButtons(Value: Byte);
    procedure DrawCell(Size: Byte);
    procedure IfGameOver;
    procedure ClickUp(Value: Byte);
    procedure ClickLeft(Value: Byte);
    procedure ClickDown(Value: Byte);
    procedure ClickRight(Value: Byte);
  end;

var
  FMain: TFMain;
  
implementation

uses uFDlgRes, uFDlgName, uFDlgHelp, uFDlgAbout;

{$R *.dfm}
{$R winxpstyle.res}


procedure TFMain.ChangeGlyph(Value: Byte);
var
  im: TBitmap;
begin
  im:=TBitmap.Create;
  case Value of
    4:
      begin
        Imagelist2.GetBitmap(2,im);
        SpeedButton02_2.Glyph:=im;
        Imagelist2.GetBitmap(3,im);
        SpeedButton03_3.Glyph:=im;
        Imagelist2.GetBitmap(5,im);
        SpeedButton13_4.Glyph:=im;
      end;
    5:
      begin
        Imagelist2.GetBitmap(2,im);
        SpeedButton02_2.Glyph:=im;
        Imagelist2.GetBitmap(3,im);
        SpeedButton03_3.Glyph:=im;
        Imagelist2.GetBitmap(4,im);
        SpeedButton13_4.Glyph:=im;
        Imagelist2.GetBitmap(5,im);
        SpeedButton14_5.Glyph:=im;
      end;
    6:
      begin
        Imagelist2.GetBitmap(2,im);
        SpeedButton02_2.Glyph:=im;
        Imagelist2.GetBitmap(3,im);
        SpeedButton03_3.Glyph:=im;
        Imagelist2.GetBitmap(4,im);
        SpeedButton13_4.Glyph:=im;
        Imagelist2.GetBitmap(5,im);
        SpeedButton14_5.Glyph:=im;
        Imagelist2.GetBitmap(6,im);
        SpeedButton01_6.Glyph:=im;
      end;
    7:
      begin
        Imagelist2.GetBitmap(1,im);
        SpeedButton02_2.Glyph:=im;
        Imagelist2.GetBitmap(2,im);
        SpeedButton03_3.Glyph:=im;
        Imagelist2.GetBitmap(3,im);
        SpeedButton13_4.Glyph:=im;
        Imagelist2.GetBitmap(4,im);
        SpeedButton14_5.Glyph:=im;
        Imagelist2.GetBitmap(5,im);
        SpeedButton01_6.Glyph:=im;
      end;
  end;
end;

procedure TFMain.HideButtons(Value: Byte);
begin
  case Value of
    4:
      begin
        SpeedButton13_4.Visible:=True;
        SpeedButton14_5.Visible:=False;
        SpeedButton48_1.Visible:=True;
        SpeedButton36_1.Visible:=False;
        SpeedButton47_2.Visible:=True;
        SpeedButton37_2.Visible:=False;
        SpeedButton46_3.Visible:=True;
        SpeedButton38_3.Visible:=False;
        SpeedButton16_4.Visible:=True;
        SpeedButton45_4.Visible:=True;
        SpeedButton39_4.Visible:=False;
        SpeedButton17_5.Visible:=False;
        SpeedButton41_1.Visible:=True;
        SpeedButton42_2.Visible:=True;
        SpeedButton43_3.Visible:=True;
        SpeedButton44_4.Visible:=True;
        SpeedButton40_5.Visible:=False;
        SpeedButton31_1.Visible:=False;
        SpeedButton32_2.Visible:=False;
        SpeedButton33_3.Visible:=False;
        SpeedButton34_4.Visible:=False;
        SpeedButton35_5.Visible:=False;
        SpeedButton01_6.Visible:=False;
        SpeedButton02_1.Visible:=False;
        SpeedButton03_2.Visible:=False;
        SpeedButton04_3.Visible:=False;
        SpeedButton05_4.Visible:=False;
        SpeedButton06_5.Visible:=False;
        SpeedButton07_6.Visible:=False;
        SpeedButton08_6.Visible:=False;
        SpeedButton07_5.Visible:=False;
        SpeedButton06_4.Visible:=False;
        SpeedButton05_3.Visible:=False;
        SpeedButton04_2.Visible:=False;
        SpeedButton03_1.Visible:=False;
        SpeedButton02_6.Visible:=False;
        SpeedButton51_1.Visible:=False;
        SpeedButton51_2.Visible:=False;
        SpeedButton51_3.Visible:=False;
        SpeedButton51_4.Visible:=False;
        SpeedButton51_5.Visible:=False;
        SpeedButton51_6.Visible:=False;
        SpeedButton51_7.Visible:=False;
        SpeedButton52_1.Visible:=False;
        SpeedButton52_2.Visible:=False;
        SpeedButton52_3.Visible:=False;
        SpeedButton52_4.Visible:=False;
        SpeedButton52_5.Visible:=False;
        SpeedButton52_6.Visible:=False;
        SpeedButton52_7.Visible:=False;
        SpeedButton01_7.Visible:=False;
        SpeedButton02_7.Visible:=False;
      end;
    5:
      begin
        SpeedButton13_4.Visible:=True;
        SpeedButton14_5.Visible:=True;
        SpeedButton48_1.Visible:=False;
        SpeedButton36_1.Visible:=True;
        SpeedButton47_2.Visible:=False;
        SpeedButton37_2.Visible:=True;
        SpeedButton46_3.Visible:=False;
        SpeedButton38_3.Visible:=True;
        SpeedButton16_4.Visible:=True;
        SpeedButton45_4.Visible:=False;
        SpeedButton39_4.Visible:=True;
        SpeedButton17_5.Visible:=True;
        SpeedButton41_1.Visible:=False;
        SpeedButton42_2.Visible:=False;
        SpeedButton43_3.Visible:=False;
        SpeedButton44_4.Visible:=False;
        SpeedButton40_5.Visible:=True;
        SpeedButton31_1.Visible:=True;
        SpeedButton32_2.Visible:=True;
        SpeedButton33_3.Visible:=True;
        SpeedButton34_4.Visible:=True;
        SpeedButton35_5.Visible:=True;
        SpeedButton01_6.Visible:=False;
        SpeedButton02_1.Visible:=False;
        SpeedButton03_2.Visible:=False;
        SpeedButton04_3.Visible:=False;
        SpeedButton05_4.Visible:=False;
        SpeedButton06_5.Visible:=False;
        SpeedButton07_6.Visible:=False;
        SpeedButton08_6.Visible:=False;
        SpeedButton07_5.Visible:=False;
        SpeedButton06_4.Visible:=False;
        SpeedButton05_3.Visible:=False;
        SpeedButton04_2.Visible:=False;
        SpeedButton03_1.Visible:=False;
        SpeedButton02_6.Visible:=False;
        SpeedButton51_1.Visible:=False;
        SpeedButton51_2.Visible:=False;
        SpeedButton51_3.Visible:=False;
        SpeedButton51_4.Visible:=False;
        SpeedButton51_5.Visible:=False;
        SpeedButton51_6.Visible:=False;
        SpeedButton51_7.Visible:=False;
        SpeedButton52_1.Visible:=False;
        SpeedButton52_2.Visible:=False;
        SpeedButton52_3.Visible:=False;
        SpeedButton52_4.Visible:=False;
        SpeedButton52_5.Visible:=False;
        SpeedButton52_6.Visible:=False;
        SpeedButton52_7.Visible:=False;
        SpeedButton01_7.Visible:=False;
        SpeedButton02_7.Visible:=False;
      end;
    6:
      begin
        SpeedButton13_4.Visible:=True;
        SpeedButton14_5.Visible:=True;
        SpeedButton48_1.Visible:=False;
        SpeedButton36_1.Visible:=False;
        SpeedButton47_2.Visible:=False;
        SpeedButton37_2.Visible:=False;
        SpeedButton46_3.Visible:=False;
        SpeedButton38_3.Visible:=False;
        SpeedButton16_4.Visible:=True;
        SpeedButton45_4.Visible:=False;
        SpeedButton39_4.Visible:=False;
        SpeedButton17_5.Visible:=True;
        SpeedButton41_1.Visible:=False;
        SpeedButton42_2.Visible:=False;
        SpeedButton43_3.Visible:=False;
        SpeedButton44_4.Visible:=False;
        SpeedButton40_5.Visible:=False;
        SpeedButton31_1.Visible:=False;
        SpeedButton32_2.Visible:=False;
        SpeedButton33_3.Visible:=False;
        SpeedButton34_4.Visible:=False;
        SpeedButton35_5.Visible:=False;
        SpeedButton01_6.Visible:=True;
        SpeedButton02_1.Visible:=True;
        SpeedButton03_2.Visible:=True;
        SpeedButton04_3.Visible:=True;
        SpeedButton05_4.Visible:=True;
        SpeedButton06_5.Visible:=True;
        SpeedButton07_6.Visible:=True;
        SpeedButton08_6.Visible:=True;
        SpeedButton07_5.Visible:=True;
        SpeedButton06_4.Visible:=True;
        SpeedButton05_3.Visible:=True;
        SpeedButton04_2.Visible:=True;
        SpeedButton03_1.Visible:=True;
        SpeedButton02_6.Visible:=True;
        SpeedButton51_1.Visible:=False;
        SpeedButton51_2.Visible:=False;
        SpeedButton51_3.Visible:=False;
        SpeedButton51_4.Visible:=False;
        SpeedButton51_5.Visible:=False;
        SpeedButton51_6.Visible:=False;
        SpeedButton51_7.Visible:=False;
        SpeedButton52_1.Visible:=False;
        SpeedButton52_2.Visible:=False;
        SpeedButton52_3.Visible:=False;
        SpeedButton52_4.Visible:=False;
        SpeedButton52_5.Visible:=False;
        SpeedButton52_6.Visible:=False;
        SpeedButton52_7.Visible:=False;
        SpeedButton01_7.Visible:=False;
        SpeedButton02_7.Visible:=False;
      end;
    7:
      begin
        SpeedButton13_4.Visible:=True;
        SpeedButton14_5.Visible:=True;
        SpeedButton48_1.Visible:=False;
        SpeedButton36_1.Visible:=False;
        SpeedButton47_2.Visible:=False;
        SpeedButton37_2.Visible:=False;
        SpeedButton46_3.Visible:=False;
        SpeedButton38_3.Visible:=False;
        SpeedButton16_4.Visible:=True;
        SpeedButton45_4.Visible:=False;
        SpeedButton39_4.Visible:=False;
        SpeedButton17_5.Visible:=True;
        SpeedButton41_1.Visible:=False;
        SpeedButton42_2.Visible:=False;
        SpeedButton43_3.Visible:=False;
        SpeedButton44_4.Visible:=False;
        SpeedButton40_5.Visible:=False;
        SpeedButton31_1.Visible:=False;
        SpeedButton32_2.Visible:=False;
        SpeedButton33_3.Visible:=False;
        SpeedButton34_4.Visible:=False;
        SpeedButton35_5.Visible:=False;
        SpeedButton01_6.Visible:=True;
        SpeedButton02_1.Visible:=False;
        SpeedButton03_2.Visible:=False;
        SpeedButton04_3.Visible:=False;
        SpeedButton05_4.Visible:=False;
        SpeedButton06_5.Visible:=False;
        SpeedButton07_6.Visible:=False;
        SpeedButton08_6.Visible:=False;
        SpeedButton07_5.Visible:=False;
        SpeedButton06_4.Visible:=False;
        SpeedButton05_3.Visible:=False;
        SpeedButton04_2.Visible:=False;
        SpeedButton03_1.Visible:=False;
        SpeedButton02_6.Visible:=True;
        SpeedButton51_1.Visible:=True;
        SpeedButton51_2.Visible:=True;
        SpeedButton51_3.Visible:=True;
        SpeedButton51_4.Visible:=True;
        SpeedButton51_5.Visible:=True;
        SpeedButton51_6.Visible:=True;
        SpeedButton51_7.Visible:=True;
        SpeedButton52_1.Visible:=True;
        SpeedButton52_2.Visible:=True;
        SpeedButton52_3.Visible:=True;
        SpeedButton52_4.Visible:=True;
        SpeedButton52_5.Visible:=True;
        SpeedButton52_6.Visible:=True;
        SpeedButton52_7.Visible:=True;
        SpeedButton01_7.Visible:=True;
        SpeedButton02_7.Visible:=True;
      end;
  end;
end;

procedure TFMain.DrawCell(Size: Byte);
var
  x, y, Rand: Byte;
  red, gr, yel, blu, lblu, mag, oran: Byte;
begin
  blu:=0;
  red:=0;
  gr:=0;
  yel:=0;
  lblu:=0;
  mag:=0;
  oran:=0;
  LastGame:=CurGame;
  CurGame:=Size;
  Winner:=False;
  if CurGame <> LastGame then
  begin
    Image1.Width:=210-30*(7-Size);
    Image1.Height:=Image1.Width;
    Bevel1.Width:=296-30*(7-Size);
    Bevel1.Height:=293-30*(7-Size);
    ClientWidth:=317-30*(7-Size);
    ClientHeight:=ClientWidth + 30;
    Label_min.Left:=ClientWidth div 2 - 48;
    Label_sep.Left:=ClientWidth div 2 - 8;
    Label_sec.Left:=ClientWidth div 2 + 8;
    HideButtons(Size);
    ChangeGlyph(Size);
  end;
  Randomize;
  bmp:=TBitmap.Create;
  for x:=0 to Size-1 do
    for y:=0 to Size-1 do
      case Size of
        4:
          begin
            repeat
              Rand:=Random(Size);
            until((Rand=0)and(blu<4))or((Rand=1)and(red<4))or((Rand=2)and(gr<4))or((Rand=3)and(yel<4));
            Imagelist1.GetBitmap(Rand,bmp);
            Image1.Canvas.Draw(x*30,y*30,bmp);
            a7[x+1,y+1]:=Rand;
            case Rand of
              0: blu:=blu+1;
              1: red:=red+1;
              2: gr:=gr+1;
              3: yel:=yel+1;
            end;
        end;
        5:
          begin
            repeat
              Rand:=Random(Size);
            until((Rand=0)and(blu<5))or((Rand=1)and(red<5))or((Rand=2)and(gr<5))or((Rand=3)and(yel<5))or((Rand=4)and(lblu<5));
            Imagelist1.GetBitmap(Rand,bmp);
            Image1.Canvas.Draw(x*30,y*30,bmp);
            a7[x+1,y+1]:=Rand;
            case Rand of
              0: blu:=blu+1;
              1: red:=red+1;
              2: gr:=gr+1;
              3: yel:=yel+1;
              4: lblu:=lblu+1;
            end;
          end;
      6:
          begin
            repeat
              Rand:=Random(Size);
            until((Rand=0)and(blu<6))or((Rand=1)and(red<6))or((Rand=2)and(gr<6))or((Rand=3)and(yel<6))or((Rand=4)and(lblu<6))or((Rand=5)and(mag<6));
            Imagelist1.GetBitmap(Rand,bmp);
            Image1.Canvas.Draw(x*30,y*30,bmp);
            a7[x+1,y+1]:=Rand;
            case Rand of
              0: blu:=blu+1;
              1: red:=red+1;
              2: gr:=gr+1;
              3: yel:=yel+1;
              4: lblu:=lblu+1;
              5: mag:=mag+1;
              end;
          end;
      7:
          begin
            repeat
              Rand:=Random(Size);
            until((Rand=0)and(blu<7))or((Rand=1)and(red<7))or((Rand=2)and(gr<7))or((Rand=3)and(yel<7))or((Rand=4)and(lblu<7))or((Rand=5)and(mag<7))or((Rand=6)and(oran<7));
            Imagelist1.GetBitmap(Rand,bmp);
            Image1.Canvas.Draw(x*30,y*30,bmp);
            a7[x+1,y+1]:=Rand;
            case Rand of
              0: blu:=blu+1;
              1: red:=red+1;
              2: gr:=gr+1;
              3: yel:=yel+1;
              4: lblu:=lblu+1;
              5: mag:=mag+1;
              6: oran:=oran+1;
            end;
          end;
      end;
end;

procedure TFMain.IfGameOver;
begin
  case Image1.Height of
    120:
      if  (a7[1,1]=1)and(a7[1,1]=a7[1,2])and(a7[1,1]=a7[1,3])and(a7[1,1]=a7[1,4])and
          (a7[2,1]=3)and(a7[2,1]=a7[2,2])and(a7[2,1]=a7[2,3])and(a7[2,1]=a7[2,4])and
          (a7[3,1]=2)and(a7[3,1]=a7[3,2])and(a7[3,1]=a7[3,3])and(a7[3,1]=a7[3,4])and
          (a7[4,1]=0)and(a7[4,1]=a7[4,2])and(a7[4,1]=a7[4,3])and(a7[4,1]=a7[4,4])then
          begin
            Timer1.Enabled:=False;
            Winner:=True;
            Application.MessageBox('�� ��������!',PChar(Caption),MB_ICONINFORMATION+MB_OK);
            CheckPlace(4);
            FDlgRes.ShowModal;
          end;
    150:
      if  (a7[1,1]=1)and(a7[1,1]=a7[1,2])and(a7[1,1]=a7[1,3])and(a7[1,1]=a7[1,4])and(a7[1,1]=a7[1,5])and
          (a7[2,1]=3)and(a7[2,1]=a7[2,2])and(a7[2,1]=a7[2,3])and(a7[2,1]=a7[2,4])and(a7[2,1]=a7[2,5])and
          (a7[3,1]=2)and(a7[3,1]=a7[3,2])and(a7[3,1]=a7[3,3])and(a7[3,1]=a7[3,4])and(a7[3,1]=a7[3,5])and
          (a7[4,1]=4)and(a7[4,1]=a7[4,2])and(a7[4,1]=a7[4,3])and(a7[4,1]=a7[4,4])and(a7[4,1]=a7[4,5])and
          (a7[5,1]=0)and(a7[5,1]=a7[5,2])and(a7[5,1]=a7[5,3])and(a7[5,1]=a7[5,4])and(a7[5,1]=a7[5,5])then
          begin
            Timer1.Enabled:=False;
            Winner:=True;
            Application.MessageBox('�� ��������!',PChar(Caption),MB_ICONINFORMATION+MB_OK);
            CheckPlace(5);
            FDlgRes.ShowModal;
          end;
    180:
      if  (a7[1,1]=1)and(a7[1,1]=a7[1,2])and(a7[1,1]=a7[1,3])and(a7[1,1]=a7[1,4])and(a7[1,1]=a7[1,5])and(a7[1,1]=a7[1,6])and
          (a7[2,1]=3)and(a7[2,1]=a7[2,2])and(a7[2,1]=a7[2,3])and(a7[2,1]=a7[2,4])and(a7[2,1]=a7[2,5])and(a7[2,1]=a7[2,6])and
          (a7[3,1]=2)and(a7[3,1]=a7[3,2])and(a7[3,1]=a7[3,3])and(a7[3,1]=a7[3,4])and(a7[3,1]=a7[3,5])and(a7[3,1]=a7[3,6])and
          (a7[4,1]=4)and(a7[4,1]=a7[4,2])and(a7[4,1]=a7[4,3])and(a7[4,1]=a7[4,4])and(a7[4,1]=a7[4,5])and(a7[4,1]=a7[4,6])and
          (a7[5,1]=0)and(a7[5,1]=a7[5,2])and(a7[5,1]=a7[5,3])and(a7[5,1]=a7[5,4])and(a7[5,1]=a7[5,5])and(a7[5,1]=a7[5,6])and
          (a7[6,1]=5)and(a7[6,1]=a7[6,2])and(a7[6,1]=a7[6,3])and(a7[6,1]=a7[6,4])and(a7[6,1]=a7[6,5])and(a7[6,1]=a7[6,6])then
          begin
            Timer1.Enabled:=False;
            Winner:=True;
            Application.MessageBox('�� ��������!',PChar(Caption),MB_ICONINFORMATION+MB_OK);
            CheckPlace(6);
            FDlgRes.ShowModal;
          end;
    210:
      if  (a7[1,1]=1)and(a7[1,1]=a7[1,2])and(a7[1,1]=a7[1,3])and(a7[1,1]=a7[1,4])and(a7[1,1]=a7[1,5])and(a7[1,1]=a7[1,6])and(a7[1,1]=a7[1,7])and
          (a7[2,1]=6)and(a7[2,1]=a7[2,2])and(a7[2,1]=a7[2,3])and(a7[2,1]=a7[2,4])and(a7[2,1]=a7[2,5])and(a7[2,1]=a7[2,6])and(a7[2,1]=a7[2,7])and
          (a7[3,1]=3)and(a7[3,1]=a7[3,2])and(a7[3,1]=a7[3,3])and(a7[3,1]=a7[3,4])and(a7[3,1]=a7[3,5])and(a7[3,1]=a7[3,6])and(a7[3,1]=a7[3,7])and
          (a7[4,1]=2)and(a7[4,1]=a7[4,2])and(a7[4,1]=a7[4,3])and(a7[4,1]=a7[4,4])and(a7[4,1]=a7[4,5])and(a7[4,1]=a7[4,6])and(a7[4,1]=a7[4,7])and
          (a7[5,1]=4)and(a7[5,1]=a7[5,2])and(a7[5,1]=a7[5,3])and(a7[5,1]=a7[5,4])and(a7[5,1]=a7[5,5])and(a7[5,1]=a7[5,6])and(a7[5,1]=a7[5,7])and
          (a7[6,1]=0)and(a7[6,1]=a7[6,2])and(a7[6,1]=a7[6,3])and(a7[6,1]=a7[6,4])and(a7[6,1]=a7[6,5])and(a7[6,1]=a7[6,6])and(a7[6,1]=a7[6,7])and
          (a7[7,1]=5)and(a7[7,1]=a7[7,2])and(a7[7,1]=a7[7,3])and(a7[7,1]=a7[7,4])and(a7[7,1]=a7[7,5])and(a7[7,1]=a7[7,6])and(a7[7,1]=a7[7,7])then
          begin
            Timer1.Enabled:=False;
            Winner:=True;
            Application.MessageBox('�� ��������!',PChar(Caption),MB_ICONINFORMATION+MB_OK);
            CheckPlace(7);
            FDlgRes.ShowModal;
          end;
  end;
end;

procedure TFMain.ClickUp(Value: Byte);
var
  i, Temp: Byte;
begin
  case Image1.Height of
    120:
      begin
        Temp:=a7[Value,1];
        a7[Value,1]:=a7[Value,2];
        a7[Value,2]:=a7[Value,3];
        a7[Value,3]:=a7[Value,4];
        a7[Value,4]:=Temp;
        for i:=0 to 3 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[Value,i+1],bmp);
          Image1.Canvas.Draw((Value-1)*30,i*30,bmp);
        end;
        IfGameOver;
      end;
    150:
      begin
        Temp:=a7[Value,1];
        a7[Value,1]:=a7[Value,2];
        a7[Value,2]:=a7[Value,3];
        a7[Value,3]:=a7[Value,4];
        a7[Value,4]:=a7[Value,5];
        a7[Value,5]:=Temp;
        for i:=0 to 4 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[Value,i+1],bmp);
          Image1.Canvas.Draw((Value-1)*30,i*30,bmp);
        end;
        IfGameOver;
      end;
    180:
      begin
        Temp:=a7[Value,1];
        a7[Value,1]:=a7[Value,2];
        a7[Value,2]:=a7[Value,3];
        a7[Value,3]:=a7[Value,4];
        a7[Value,4]:=a7[Value,5];
        a7[Value,5]:=a7[Value,6];
        a7[Value,6]:=Temp;
        for i:=0 to 5 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[Value,i+1],bmp);
          Image1.Canvas.Draw((Value-1)*30,i*30,bmp);
        end;
        IfGameOver;
      end;
    210:
      begin
        Temp:=a7[Value,1];
        a7[Value,1]:=a7[Value,2];
        a7[Value,2]:=a7[Value,3];
        a7[Value,3]:=a7[Value,4];
        a7[Value,4]:=a7[Value,5];
        a7[Value,5]:=a7[Value,6];
        a7[Value,6]:=a7[Value,7];
        a7[Value,7]:=Temp;
        for i:=0 to 6 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[Value,i+1],bmp);
          Image1.Canvas.Draw((Value-1)*30,i*30,bmp);
        end;
        IfGameOver;
      end;
  end;
end;

procedure TFMain.ClickLeft(Value: Byte);
var
  i, Temp: Byte;
begin
  case Image1.Height of
    120:
      begin
        Temp:=a7[1,Value];
        a7[1,Value]:=a7[2,Value];
        a7[2,Value]:=a7[3,Value];
        a7[3,Value]:=a7[4,Value];
        a7[4,Value]:=Temp;
        for i:=0 to 3 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[i+1,Value],bmp);
          Image1.Canvas.Draw(i*30,(Value-1)*30,bmp);
        end;
        IfGameOver;
      end;
    150:
      begin
        Temp:=a7[1,Value];
        a7[1,Value]:=a7[2,Value];
        a7[2,Value]:=a7[3,Value];
        a7[3,Value]:=a7[4,Value];
        a7[4,Value]:=a7[5,Value];
        a7[5,Value]:=Temp;
        for i:=0 to 4 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[i+1,Value],bmp);
          Image1.Canvas.Draw(i*30,(Value-1)*30,bmp);
        end;
        IfGameOver;
      end;
    180:
      begin
        Temp:=a7[1,Value];
        a7[1,Value]:=a7[2,Value];
        a7[2,Value]:=a7[3,Value];
        a7[3,Value]:=a7[4,Value];
        a7[4,Value]:=a7[5,Value];
        a7[5,Value]:=a7[6,Value];
        a7[6,Value]:=Temp;
        for i:=0 to 5 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[i+1,Value],bmp);
          Image1.Canvas.Draw(i*30,(Value-1)*30,bmp);
        end;
        IfGameOver;
      end;
    210:
      begin
        Temp:=a7[1,Value];
        a7[1,Value]:=a7[2,Value];
        a7[2,Value]:=a7[3,Value];
        a7[3,Value]:=a7[4,Value];
        a7[4,Value]:=a7[5,Value];
        a7[5,Value]:=a7[6,Value];
        a7[6,Value]:=a7[7,Value];
        a7[7,Value]:=Temp;
        for i:=0 to 6 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[i+1,Value],bmp);
          Image1.Canvas.Draw(i*30,(Value-1)*30,bmp);
        end;
        IfGameOver;
      end;
  end;
end;

procedure TFMain.ClickDown(Value: Byte);
var
  i, Temp: Byte;
begin
  case Image1.Height of
    120:
      begin
        Temp:=a7[Value,4];
        a7[Value,4]:=a7[Value,3];
        a7[Value,3]:=a7[Value,2];
        a7[Value,2]:=a7[Value,1];
        a7[Value,1]:=Temp;
        for i:=0 to 3 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[Value,i+1],bmp);
          Image1.Canvas.Draw((Value-1)*30,i*30,bmp);
        end;
        IfGameOver;
      end;
    150:
      begin
        Temp:=a7[Value,5];
        a7[Value,5]:=a7[Value,4];
        a7[Value,4]:=a7[Value,3];
        a7[Value,3]:=a7[Value,2];
        a7[Value,2]:=a7[Value,1];
        a7[Value,1]:=Temp;
        for i:=0 to 4 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[Value,i+1],bmp);
          Image1.Canvas.Draw((Value-1)*30,i*30,bmp);
        end;
        IfGameOver;
      end;
    180:
      begin
        Temp:=a7[Value,6];
        a7[Value,6]:=a7[Value,5];
        a7[Value,5]:=a7[Value,4];
        a7[Value,4]:=a7[Value,3];
        a7[Value,3]:=a7[Value,2];
        a7[Value,2]:=a7[Value,1];
        a7[Value,1]:=Temp;
        for i:=0 to 5 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[Value,i+1],bmp);
          Image1.Canvas.Draw((Value-1)*30,i*30,bmp);
        end;
        IfGameOver;
      end;
    210:
      begin
        Temp:=a7[Value,7];
        a7[Value,7]:=a7[Value,6];
        a7[Value,6]:=a7[Value,5];
        a7[Value,5]:=a7[Value,4];
        a7[Value,4]:=a7[Value,3];
        a7[Value,3]:=a7[Value,2];
        a7[Value,2]:=a7[Value,1];
        a7[Value,1]:=Temp;
        for i:=0 to 6 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[Value,i+1],bmp);
          Image1.Canvas.Draw((Value-1)*30,i*30,bmp);
        end;
        IfGameOver;
      end;
  end;
end;

procedure TFMain.ClickRight(Value: Byte);
var
  i, Temp: Byte;
begin
  case Image1.Height of
    120:
      begin
        Temp:=a7[4,Value];
        a7[4,Value]:=a7[3,Value];
        a7[3,Value]:=a7[2,Value];
        a7[2,Value]:=a7[1,Value];
        a7[1,Value]:=Temp;
        for i:=0 to 3 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[i+1,Value],bmp);
          Image1.Canvas.Draw(i*30,(Value-1)*30,bmp);
        end;
        IfGameOver;
      end;
    150:
      begin
        Temp:=a7[5,Value];
        a7[5,Value]:=a7[4,Value];
        a7[4,Value]:=a7[3,Value];
        a7[3,Value]:=a7[2,Value];
        a7[2,Value]:=a7[1,Value];
        a7[1,Value]:=Temp;
        for i:=0 to 4 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[i+1,Value],bmp);
          Image1.Canvas.Draw(i*30,(Value-1)*30,bmp);
        end;
        IfGameOver;
      end;
    180:
      begin
        Temp:=a7[6,Value];
        a7[6,Value]:=a7[5,Value];
        a7[5,Value]:=a7[4,Value];
        a7[4,Value]:=a7[3,Value];
        a7[3,Value]:=a7[2,Value];
        a7[2,Value]:=a7[1,Value];
        a7[1,Value]:=Temp;
        for i:=0 to 5 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[i+1,Value],bmp);
          Image1.Canvas.Draw(i*30,(Value-1)*30,bmp);
        end;
        IfGameOver;
      end;
    210:
      begin
        Temp:=a7[7,Value];
        a7[7,Value]:=a7[6,Value];
        a7[6,Value]:=a7[5,Value];
        a7[5,Value]:=a7[4,Value];
        a7[4,Value]:=a7[3,Value];
        a7[3,Value]:=a7[2,Value];
        a7[2,Value]:=a7[1,Value];
        a7[1,Value]:=Temp;
        for i:=0 to 6 do
        begin
          bmp:=TBitmap.Create;
          Imagelist1.GetBitmap(a7[i+1,Value],bmp);
          Image1.Canvas.Draw(i*30,(Value-1)*30,bmp);
        end;
        IfGameOver;
      end;
  end;
end;

procedure TFMain.SpeedButton01_1Click(Sender: TObject);
begin
  if not Winner then
    ClickUp(strtoint((sender as TSpeedButton).name[15]));
end;

procedure TFMain.SpeedButton12_1Click(Sender: TObject);
begin
  if not Winner then
    ClickLeft(strtoint((sender as TSpeedButton).name[15]));
end;

procedure TFMain.SpeedButton09_1Click(Sender: TObject);
begin
  if not Winner then
    ClickDown(strtoint((sender as TSpeedButton).name[15]));
end;

procedure TFMain.SpeedButton04_1Click(Sender: TObject);
begin
  if not Winner then
    ClickRight(strtoint((sender as TSpeedButton).name[15]));
end;

procedure TFMain.FormCreate(Sender: TObject);
begin
  DrawCell(7);
end;

procedure TFMain.N44Click(Sender: TObject);
begin
  N44.Checked:=True;
  N55.Checked:=False;
  N66.Checked:=False;
  N77.Checked:=False;
  DrawCell(4);
end;

procedure TFMain.N55Click(Sender: TObject);
begin
  N44.Checked:=False;
  N55.Checked:=True;
  N66.Checked:=False;
  N77.Checked:=False;
  DrawCell(5);
end;

procedure TFMain.N66Click(Sender: TObject);
begin
  N44.Checked:=False;
  N55.Checked:=False;
  N66.Checked:=True;
  N77.Checked:=False;
  DrawCell(6);
end;

procedure TFMain.N77Click(Sender: TObject);
begin
  N44.Checked:=False;
  N55.Checked:=False;
  N66.Checked:=False;
  N77.Checked:=True;
  DrawCell(7);
end;

procedure TFMain.N11Click(Sender: TObject);
begin
  if FileExists(ExtractFilePath(ParamStr(0))+'Rainbow2D.exe')then
  begin
    ShellExecute(0,'',PChar(ExtractFilePath(ParamStr(0))+'Rainbow2D.exe'),'','',1);
    Close;
  end
  else
    Application.MessageBox(PChar('���� �� ������'#10#13+ExtractFilePath(ParamStr(0))+'Rainbow2D.exe'),PChar(Caption),mb_ok+MB_ICONERROR);
end;

procedure TFMain.N4Click(Sender: TObject);
begin
  Close;
end;

procedure TFMain.Timer1Timer(Sender: TObject);
begin
  label_sec.caption:=inttostr(strtoint(label_sec.Caption)+1);
  if length(label_sec.caption) = 1 then
    label_sec.caption:='0' + label_sec.caption;
  if label_sec.caption='60' then
  begin
    label_sec.caption:='00';
    label_min.Caption:=inttostr(strtoint(label_min.Caption)+1);
    if length(label_min.caption) = 1 then
      label_min.caption:='0' + label_min.caption;
  end;
end;

procedure TFMain.CheckPlace(GameType: Byte);
begin
  case GameType of
    4:
      if (strtoint(label_min.Caption)<=strtoint(copy(FDlgRes.Res.FP4.Time,1,2)))and(strtoint(label_sec.Caption)<=strtoint(copy(FDlgRes.Res.FP4.Time,4,2))) then
      begin
        FDlgName.ShowModal;
        FDlgRes.Res.FP4.Name:=FDlgName.Edit1.Text;
        FDlgRes.Res.FP4.Time:=Label_min.Caption+Label_sep.Caption+Label_sec.Caption;
      end;
    5:
      if (strtoint(label_min.Caption)<=strtoint(copy(FDlgRes.Res.FP5.Time,1,2)))and(strtoint(label_sec.Caption)<=strtoint(copy(FDlgRes.Res.FP5.Time,4,2))) then
      begin
        FDlgName.ShowModal;
        FDlgRes.Res.FP5.Name:=FDlgName.Edit1.Text;
        FDlgRes.Res.FP5.Time:=Label_min.Caption+Label_sep.Caption+Label_sec.Caption;
      end;
    6:
      if (strtoint(label_min.Caption)<=strtoint(copy(FDlgRes.Res.FP6.Time,1,2)))and(strtoint(label_sec.Caption)<=strtoint(copy(FDlgRes.Res.FP6.Time,4,2))) then
      begin
        FDlgName.ShowModal;
        FDlgRes.Res.FP6.Name:=FDlgName.Edit1.Text;
        FDlgRes.Res.FP6.Time:=Label_min.Caption+Label_sep.Caption+Label_sec.Caption;
      end;
    7:
      if (strtoint(label_min.Caption)<=strtoint(copy(FDlgRes.Res.FP7.Time,1,2)))and(strtoint(label_sec.Caption)<=strtoint(copy(FDlgRes.Res.FP7.Time,4,2))) {Count<FDlgRes.Res.FP7.Count} then
      begin
        FDlgName.ShowModal;
        FDlgRes.Res.FP7.Name:=FDlgName.Edit1.Text;
        FDlgRes.Res.FP7.Time:=Label_min.Caption+Label_sep.Caption+Label_sec.Caption;
      end;
  end;
end;

procedure TFMain.N6Click(Sender: TObject);
begin
  FDlgRes.ShowModal;
end;

procedure TFMain.N7Click(Sender: TObject);
begin
  FDlgHelp.ShowModal;
end;

procedure TFMain.N3Click(Sender: TObject);
begin
  FDlgAbout.ShowModal;
end;

end.
