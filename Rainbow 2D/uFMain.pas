unit uFMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ImgList, Buttons, Grids, DBGrids, Menus,
  ShellAPI;

type
  TFMain = class(TForm)
    ImageList1: TImageList;
    Label_min: TLabel;
    Timer1: TTimer;
    Label_sep: TLabel;
    Label_sec: TLabel;
    ImageList2: TImageList;
    Image1: TImage;
    SpeedButton01_1: TSpeedButton;
    SpeedButton02_2: TSpeedButton;
    SpeedButton03_3: TSpeedButton;
    SpeedButton10_3: TSpeedButton;
    SpeedButton11_2: TSpeedButton;
    SpeedButton12_1: TSpeedButton;
    SpeedButton13_4: TSpeedButton;
    SpeedButton14_5: TSpeedButton;
    SpeedButton16_4: TSpeedButton;
    SpeedButton17_5: TSpeedButton;
    SpeedButton31_1: TSpeedButton;
    SpeedButton32_2: TSpeedButton;
    SpeedButton33_3: TSpeedButton;
    SpeedButton34_4: TSpeedButton;
    SpeedButton35_5: TSpeedButton;
    SpeedButton36_1: TSpeedButton;
    SpeedButton37_2: TSpeedButton;
    SpeedButton38_3: TSpeedButton;
    SpeedButton39_4: TSpeedButton;
    SpeedButton40_5: TSpeedButton;
    SpeedButton41_1: TSpeedButton;
    SpeedButton42_2: TSpeedButton;
    SpeedButton43_3: TSpeedButton;
    SpeedButton44_4: TSpeedButton;
    SpeedButton45_4: TSpeedButton;
    SpeedButton46_3: TSpeedButton;
    SpeedButton47_2: TSpeedButton;
    SpeedButton48_1: TSpeedButton;
    SpeedButton01_6: TSpeedButton;
    SpeedButton02_1: TSpeedButton;
    SpeedButton03_2: TSpeedButton;
    SpeedButton04_3: TSpeedButton;
    SpeedButton05_4: TSpeedButton;
    SpeedButton06_5: TSpeedButton;
    SpeedButton07_6: TSpeedButton;
    SpeedButton02_6: TSpeedButton;
    SpeedButton03_1: TSpeedButton;
    SpeedButton04_2: TSpeedButton;
    SpeedButton05_3: TSpeedButton;
    SpeedButton06_4: TSpeedButton;
    SpeedButton07_5: TSpeedButton;
    SpeedButton08_6: TSpeedButton;
    SpeedButton51_1: TSpeedButton;
    SpeedButton51_2: TSpeedButton;
    SpeedButton51_3: TSpeedButton;
    SpeedButton51_4: TSpeedButton;
    SpeedButton51_5: TSpeedButton;
    SpeedButton51_6: TSpeedButton;
    SpeedButton51_7: TSpeedButton;
    SpeedButton52_6: TSpeedButton;
    SpeedButton52_5: TSpeedButton;
    SpeedButton52_4: TSpeedButton;
    SpeedButton52_3: TSpeedButton;
    SpeedButton52_1: TSpeedButton;
    SpeedButton52_2: TSpeedButton;
    SpeedButton52_7: TSpeedButton;
    SpeedButton01_7: TSpeedButton;
    SpeedButton02_7: TSpeedButton;
    Bevel1: TBevel;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N44: TMenuItem;
    N55: TMenuItem;
    N66: TMenuItem;
    N77: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N13: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    procedure SpeedButton01_1Click(Sender: TObject);
    procedure SpeedButton12_1Click(Sender: TObject);
    procedure SpeedButton09_1Click(Sender: TObject);
    procedure SpeedButton04_1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N44Click(Sender: TObject);
    procedure N55Click(Sender: TObject);
    procedure N66Click(Sender: TObject);
    procedure N77Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
  private
    { Private declarations }
  public
    Bmp: TBitmap;
    a7: Array [1..7,1..7] of Integer;
    CurGame, LastGame: Byte;
    Winner: Boolean;
    procedure CheckPlace(GameType: Byte);
    procedure ChangeGlyph(Value: Byte);
    procedure HideButtons(Value: Byte);
    procedure DrawCell(Size: Byte);
    procedure IfGameOver;
    procedure ClickUp(Row: Byte);
    procedure ClickLeft(Line: Byte);
    procedure ClickDown(Row: Byte);
    procedure ClickRight(Line: Byte);
  end;

var
  FMain: TFMain;


implementation

uses uFDlgRes, uFDlgName, uFDlgAbout, uFDlgHelp;


{$R *.dfm}
{$R winxpstyle.res}


procedure TFMain.ChangeGlyph (Value: Byte);
var
  im: TBitmap;
begin
  im:=TBitmap.Create;
  case Value of
    4:
      begin
        ImageList2.GetBitmap(5,im);
        SpeedButton11_2.Glyph:=im;
        ImageList2.GetBitmap(7,im);
        SpeedButton10_3.Glyph:=im;
        ImageList2.GetBitmap(11,im);
        SpeedButton16_4.Glyph:=im;
        ImageList2.GetBitmap(4,im);
        SpeedButton02_2.Glyph:=im;
        ImageList2.GetBitmap(6,im);
        SpeedButton03_3.Glyph:=im;
        ImageList2.GetBitmap(10,im);
        SpeedButton13_4.Glyph:=im;
      end;
    5:
      begin
        ImageList2.GetBitmap(5,im);
        SpeedButton11_2.Glyph:=im;
        ImageList2.GetBitmap(7,im);
        SpeedButton10_3.Glyph:=im;
        ImageList2.GetBitmap(9,im);
        SpeedButton16_4.Glyph:=im;
        ImageList2.GetBitmap(11,im);
        SpeedButton17_5.Glyph:=im;
        ImageList2.GetBitmap(4,im);
        SpeedButton02_2.Glyph:=im;
        ImageList2.GetBitmap(6,im);
        SpeedButton03_3.Glyph:=im;
        ImageList2.GetBitmap(8,im);
        SpeedButton13_4.Glyph:=im;
        ImageList2.GetBitmap(10,im);
        SpeedButton14_5.Glyph:=im;
      end;
    6:
      begin
        ImageList2.GetBitmap(5,im);
        SpeedButton11_2.Glyph:=im;
        ImageList2.GetBitmap(7,im);
        SpeedButton10_3.Glyph:=im;
        ImageList2.GetBitmap(9,im);
        SpeedButton16_4.Glyph:=im;
        ImageList2.GetBitmap(11,im);
        SpeedButton17_5.Glyph:=im;
        ImageList2.GetBitmap(13,im);
        SpeedButton02_6.Glyph:=im;
        ImageList2.GetBitmap(4,im);
        SpeedButton02_2.Glyph:=im;
        ImageList2.GetBitmap(6,im);
        SpeedButton03_3.Glyph:=im;
        ImageList2.GetBitmap(8,im);
        SpeedButton13_4.Glyph:=im;
        ImageList2.GetBitmap(10,im);
        SpeedButton14_5.Glyph:=im;
        ImageList2.GetBitmap(12,im);
        SpeedButton01_6.Glyph:=im;
      end;
    7:
      begin
        ImageList2.GetBitmap(3,im);
        SpeedButton11_2.Glyph:=im;
        ImageList2.GetBitmap(5,im);
        SpeedButton10_3.Glyph:=im;
        ImageList2.GetBitmap(7,im);
        SpeedButton16_4.Glyph:=im;
        ImageList2.GetBitmap(9,im);
        SpeedButton17_5.Glyph:=im;
        ImageList2.GetBitmap(11,im);
        SpeedButton02_6.Glyph:=im;
        ImageList2.GetBitmap(2,im);
        SpeedButton02_2.Glyph:=im;
        ImageList2.GetBitmap(4,im);
        SpeedButton03_3.Glyph:=im;
        ImageList2.GetBitmap(6,im);
        SpeedButton13_4.Glyph:=im;
        ImageList2.GetBitmap(8,im);
        SpeedButton14_5.Glyph:=im;
        ImageList2.GetBitmap(10,im);
        SpeedButton01_6.Glyph:=im;
      end;
  end;
end;


procedure TFMain.HideButtons(Value: Byte);
begin
  case Value of
    4:
      begin
        speedbutton13_4.Visible:=True;
        speedbutton14_5.Visible:=False;
        speedbutton48_1.Visible:=True;
        speedbutton36_1.Visible:=False;
        speedbutton47_2.Visible:=True;
        speedbutton37_2.Visible:=False;
        speedbutton46_3.Visible:=True;
        speedbutton38_3.Visible:=False;
        speedbutton16_4.Visible:=True;
        speedbutton45_4.Visible:=True;
        speedbutton39_4.Visible:=False;
        speedbutton17_5.Visible:=False;
        speedbutton41_1.Visible:=True;
        speedbutton42_2.Visible:=True;
        speedbutton43_3.Visible:=True;
        speedbutton44_4.Visible:=True;
        speedbutton40_5.Visible:=False;
        speedbutton31_1.Visible:=False;
        speedbutton32_2.Visible:=False;
        speedbutton33_3.Visible:=False;
        speedbutton34_4.Visible:=False;
        speedbutton35_5.Visible:=False;
        speedbutton01_6.Visible:=False;
        speedbutton02_1.Visible:=False;
        speedbutton03_2.Visible:=False;
        speedbutton04_3.Visible:=False;
        speedbutton05_4.Visible:=False;
        speedbutton06_5.Visible:=False;
        speedbutton07_6.Visible:=False;
        speedbutton08_6.Visible:=False;
        speedbutton07_5.Visible:=False;
        speedbutton06_4.Visible:=False;
        speedbutton05_3.Visible:=False;
        speedbutton04_2.Visible:=False;
        speedbutton03_1.Visible:=False;
        speedbutton02_6.Visible:=False;
        speedbutton51_1.Visible:=False;
        speedbutton51_2.Visible:=False;
        speedbutton51_3.Visible:=False;
        speedbutton51_4.Visible:=False;
        speedbutton51_5.Visible:=False;
        speedbutton51_6.Visible:=False;
        speedbutton51_7.Visible:=False;
        speedbutton52_1.Visible:=False;
        speedbutton52_2.Visible:=False;
        speedbutton52_3.Visible:=False;
        speedbutton52_4.Visible:=False;
        speedbutton52_5.Visible:=False;
        speedbutton52_6.Visible:=False;
        speedbutton52_7.Visible:=False;
        speedbutton01_7.Visible:=False;
        speedbutton02_7.Visible:=False;
      end;
    5:
      begin
        speedbutton13_4.Visible:=True;
        speedbutton14_5.Visible:=True;
        speedbutton48_1.Visible:=False;
        speedbutton36_1.Visible:=True;
        speedbutton47_2.Visible:=False;
        speedbutton37_2.Visible:=True;
        speedbutton46_3.Visible:=False;
        speedbutton38_3.Visible:=True;
        speedbutton16_4.Visible:=True;
        speedbutton45_4.Visible:=False;
        speedbutton39_4.Visible:=True;
        speedbutton17_5.Visible:=True;
        speedbutton41_1.Visible:=False;
        speedbutton42_2.Visible:=False;
        speedbutton43_3.Visible:=False;
        speedbutton44_4.Visible:=False;
        speedbutton40_5.Visible:=True;
        speedbutton31_1.Visible:=True;
        speedbutton32_2.Visible:=True;
        speedbutton33_3.Visible:=True;
        speedbutton34_4.Visible:=True;
        speedbutton35_5.Visible:=True;
        speedbutton01_6.Visible:=False;
        speedbutton02_1.Visible:=False;
        speedbutton03_2.Visible:=False;
        speedbutton04_3.Visible:=False;
        speedbutton05_4.Visible:=False;
        speedbutton06_5.Visible:=False;
        speedbutton07_6.Visible:=False;
        speedbutton08_6.Visible:=False;
        speedbutton07_5.Visible:=False;
        speedbutton06_4.Visible:=False;
        speedbutton05_3.Visible:=False;
        speedbutton04_2.Visible:=False;
        speedbutton03_1.Visible:=False;
        speedbutton02_6.Visible:=False;
        speedbutton51_1.Visible:=False;
        speedbutton51_2.Visible:=False;
        speedbutton51_3.Visible:=False;
        speedbutton51_4.Visible:=False;
        speedbutton51_5.Visible:=False;
        speedbutton51_6.Visible:=False;
        speedbutton51_7.Visible:=False;
        speedbutton52_1.Visible:=False;
        speedbutton52_2.Visible:=False;
        speedbutton52_3.Visible:=False;
        speedbutton52_4.Visible:=False;
        speedbutton52_5.Visible:=False;
        speedbutton52_6.Visible:=False;
        speedbutton52_7.Visible:=False;
        speedbutton01_7.Visible:=False;
        speedbutton02_7.Visible:=False;
      end;
    6:
      begin
        speedbutton13_4.Visible:=True;
        speedbutton14_5.Visible:=True;
        speedbutton48_1.Visible:=False;
        speedbutton36_1.Visible:=False;
        speedbutton47_2.Visible:=False;
        speedbutton37_2.Visible:=False;
        speedbutton46_3.Visible:=False;
        speedbutton38_3.Visible:=False;
        speedbutton16_4.Visible:=True;
        speedbutton45_4.Visible:=False;
        speedbutton39_4.Visible:=False;
        speedbutton17_5.Visible:=True;
        speedbutton41_1.Visible:=False;
        speedbutton42_2.Visible:=False;
        speedbutton43_3.Visible:=False;
        speedbutton44_4.Visible:=False;
        speedbutton40_5.Visible:=False;
        speedbutton31_1.Visible:=False;
        speedbutton32_2.Visible:=False;
        speedbutton33_3.Visible:=False;
        speedbutton34_4.Visible:=False;
        speedbutton35_5.Visible:=False;
        speedbutton01_6.Visible:=True;
        speedbutton02_1.Visible:=True;
        speedbutton03_2.Visible:=True;
        speedbutton04_3.Visible:=True;
        speedbutton05_4.Visible:=True;
        speedbutton06_5.Visible:=True;
        speedbutton07_6.Visible:=True;
        speedbutton08_6.Visible:=True;
        speedbutton07_5.Visible:=True;
        speedbutton06_4.Visible:=True;
        speedbutton05_3.Visible:=True;
        speedbutton04_2.Visible:=True;
        speedbutton03_1.Visible:=True;
        speedbutton02_6.Visible:=True;
        speedbutton51_1.Visible:=False;
        speedbutton51_2.Visible:=False;
        speedbutton51_3.Visible:=False;
        speedbutton51_4.Visible:=False;
        speedbutton51_5.Visible:=False;
        speedbutton51_6.Visible:=False;
        speedbutton51_7.Visible:=False;
        speedbutton52_1.Visible:=False;
        speedbutton52_2.Visible:=False;
        speedbutton52_3.Visible:=False;
        speedbutton52_4.Visible:=False;
        speedbutton52_5.Visible:=False;
        speedbutton52_6.Visible:=False;
        speedbutton52_7.Visible:=False;
        speedbutton01_7.Visible:=False;
        speedbutton02_7.Visible:=False;
      end;
    7:
      begin
        speedbutton13_4.Visible:=True;
        speedbutton14_5.Visible:=True;
        speedbutton48_1.Visible:=False;
        speedbutton36_1.Visible:=False;
        speedbutton47_2.Visible:=False;
        speedbutton37_2.Visible:=False;
        speedbutton46_3.Visible:=False;
        speedbutton38_3.Visible:=False;
        speedbutton16_4.Visible:=True;
        speedbutton45_4.Visible:=False;
        speedbutton39_4.Visible:=False;
        speedbutton17_5.Visible:=True;
        speedbutton41_1.Visible:=False;
        speedbutton42_2.Visible:=False;
        speedbutton43_3.Visible:=False;
        speedbutton44_4.Visible:=False;
        speedbutton40_5.Visible:=False;
        speedbutton31_1.Visible:=False;
        speedbutton32_2.Visible:=False;
        speedbutton33_3.Visible:=False;
        speedbutton34_4.Visible:=False;
        speedbutton35_5.Visible:=False;
        speedbutton01_6.Visible:=True;
        speedbutton02_1.Visible:=False;
        speedbutton03_2.Visible:=False;
        speedbutton04_3.Visible:=False;
        speedbutton05_4.Visible:=False;
        speedbutton06_5.Visible:=False;
        speedbutton07_6.Visible:=False;
        speedbutton08_6.Visible:=False;
        speedbutton07_5.Visible:=False;
        speedbutton06_4.Visible:=False;
        speedbutton05_3.Visible:=False;
        speedbutton04_2.Visible:=False;
        speedbutton03_1.Visible:=False;
        speedbutton02_6.Visible:=True;
        speedbutton51_1.Visible:=True;
        speedbutton51_2.Visible:=True;
        speedbutton51_3.Visible:=True;
        speedbutton51_4.Visible:=True;
        speedbutton51_5.Visible:=True;
        speedbutton51_6.Visible:=True;
        speedbutton51_7.Visible:=True;
        speedbutton52_1.Visible:=True;
        speedbutton52_2.Visible:=True;
        speedbutton52_3.Visible:=True;
        speedbutton52_4.Visible:=True;
        speedbutton52_5.Visible:=True;
        speedbutton52_6.Visible:=True;
        speedbutton52_7.Visible:=True;
        speedbutton01_7.Visible:=True;
        speedbutton02_7.Visible:=True;
      end;
  end;
end;

procedure TFMain.DrawCell(Size: Byte);
var
  x, y, Rand: Byte;
  c7: Set of Byte;
begin
  case Size of
    4: c7:=[0..3,7..9,13..15,19..21,25..27];
    5: c7:=[0..4,7..10,13..16,19..22,25..28,31..34];
    6: c7:=[0..5,7..11,13..17,19..23,25..29,31..35,37..41];
    7: c7:=[0..48];
  end;
  Randomize;
  LastGame:=CurGame;
  CurGame:=Size;
  Winner:=False;
  if CurGame<>LastGame then
  begin
    Image1.Width:=210-30*(7-Size);
    Image1.Height:=Image1.Width;
    Bevel1.Width:=296-30*(7-Size);
    Bevel1.Height:=293-30*(7-Size);
    ClientWidth:=317-30*(7-Size);
    ClientHeight:=ClientWidth + 30;
    Label_min.Left:=ClientWidth div 2 - 48;
    Label_sep.Left:=ClientWidth div 2 - 8;
    Label_sec.Left:=ClientWidth div 2 + 8;
    HideButtons(Size);
    ChangeGlyph(Size);
  end;

  bmp:=TBitmap.Create;
  for x:=0 to Size-1 do
    for y:=0 to Size-1 do
    begin
      repeat
        Rand:=Random(49);
      until Rand in c7;
      ImageList1.GetBitmap(Rand,bmp);
      Image1.Canvas.Draw(x*30,y*30,bmp);
      a7[x+1,y+1]:=Rand;
      c7:=c7-[Rand];
    end;

(*
  a7[1,1]:=1;a7[1,2]:=18;a7[1,3]:=15;a7[1,4]:=14;a7[1,5]:=16;a7[1,6]:=13;a7[1,7]:=17;
  a7[2,1]:=44;a7[2,2]:=6;a7[2,3]:=46;a7[2,4]:=45;a7[2,5]:=47;a7[2,6]:=43;a7[2,7]:=48;
  a7[3,1]:=26;a7[3,2]:=30;a7[3,3]:=3;a7[3,4]:=27;a7[3,5]:=28;a7[3,6]:=25;a7[3,7]:=29;
  a7[4,1]:=20;a7[4,2]:=24;a7[4,3]:=21;a7[4,4]:=2;a7[4,5]:=22;a7[4,6]:=19;a7[4,7]:=23;
  a7[5,1]:=32;a7[5,2]:=36;a7[5,3]:=34;a7[5,4]:=33;a7[5,5]:=4;a7[5,6]:=31;a7[5,7]:=35;
  a7[6,1]:=7;a7[6,2]:=12;a7[6,3]:=9;a7[6,4]:=8;a7[6,5]:=10;a7[6,6]:=0;a7[6,7]:=11;
  a7[7,1]:=38;a7[7,2]:=42;a7[7,3]:=40;a7[7,4]:=39;a7[7,5]:=41;a7[7,6]:=37;a7[7,7]:=5;
//*)

  label_min.Caption:='00';
  label_sec.Caption:='00';
  Timer1.Enabled:=False;
  Timer1.Enabled:=True;
end;

procedure TFMain.IfGameOver;
begin
  case Image1.Height of
    120:
      if  (a7[1,1]=1)and(a7[1,2]=15)and(a7[1,3]=14)and(a7[1,4]=13)and
          (a7[2,1]=26)and(a7[2,2]=3)and(a7[2,3]=27)and(a7[2,4]=25)and
          (a7[3,1]=20)and(a7[3,2]=21)and(a7[3,3]=2)and(a7[3,4]=19)and
          (a7[4,1]=7)and(a7[4,2]=9)and(a7[4,3]=8)and(a7[4,4]=0)then
          begin
            Timer1.Enabled:=False;
            Winner:=True;
            Application.MessageBox('�� ��������!',PChar(Caption),MB_ICONINFORMATION+MB_OK);
            CheckPlace(4);
            FDlgRes.ShowModal;
          end;
    150:
      if  (a7[1,1]=1)and(a7[1,2]=15)and(a7[1,3]=14)and(a7[1,4]=16)and(a7[1,5]=13)and
          (a7[2,1]=26)and(a7[2,2]=3)and(a7[2,3]=27)and(a7[2,4]=28)and(a7[2,5]=25)and
          (a7[3,1]=20)and(a7[3,2]=21)and(a7[3,3]=2)and(a7[3,4]=22)and(a7[3,5]=19)and
          (a7[4,1]=32)and(a7[4,2]=34)and(a7[4,3]=33)and(a7[4,4]=4)and(a7[4,5]=31)and
          (a7[5,1]=7)and(a7[5,2]=9)and(a7[5,3]=8)and(a7[5,4]=10)and(a7[5,5]=0)then
          begin
            Timer1.Enabled:=False;
            Winner:= True;
            Application.MessageBox('�� ��������!',PChar(Caption),MB_ICONINFORMATION+MB_OK);
            CheckPlace(5);
            FDlgRes.ShowModal;
          end;
    180:
      if  (a7[1,1]=1)and(a7[1,2]=15)and(a7[1,3]=14)and(a7[1,4]=16)and(a7[1,5]=13)and(a7[1,6]=17)and
          (a7[2,1]=26)and(a7[2,2]=3)and(a7[2,3]=27)and(a7[2,4]=28)and(a7[2,5]=25)and(a7[2,6]=29)and
          (a7[3,1]=20)and(a7[3,2]=21)and(a7[3,3]=2)and(a7[3,4]=22)and(a7[3,5]=19)and(a7[3,6]=23)and
          (a7[4,1]=32)and(a7[4,2]=34)and(a7[4,3]=33)and(a7[4,4]=4)and(a7[4,5]=31)and(a7[4,6]=35)and
          (a7[5,1]=7)and(a7[5,2]=9)and(a7[5,3]=8)and(a7[5,4]=10)and(a7[5,5]=0)and(a7[5,6]=11)and
          (a7[6,1]=38)and(a7[6,2]=40)and(a7[6,3]=39)and(a7[6,4]=41)and(a7[6,5]=37)and(a7[6,6]=5)then
          begin
            Timer1.Enabled:=False;
            Winner:= True;
            Application.MessageBox('�� ��������!',PChar(Caption),MB_ICONINFORMATION+MB_OK);
            CheckPlace(6);
            FDlgRes.ShowModal;
          end;
    210:
      if  (a7[1,1]=1)and(a7[1,2]=18)and(a7[1,3]=15)and(a7[1,4]=14)and(a7[1,5]=16)and(a7[1,6]=13)and(a7[1,7]=17)and
          (a7[2,1]=44)and(a7[2,2]=6)and(a7[2,3]=46)and(a7[2,4]=45)and(a7[2,5]=47)and(a7[2,6]=43)and(a7[2,7]=48)and
          (a7[3,1]=26)and(a7[3,2]=30)and(a7[3,3]=3)and(a7[3,4]=27)and(a7[3,5]=28)and(a7[3,6]=25)and(a7[3,7]=29)and
          (a7[4,1]=20)and(a7[4,2]=24)and(a7[4,3]=21)and(a7[4,4]=2)and(a7[4,5]=22)and(a7[4,6]=19)and(a7[4,7]=23)and
          (a7[5,1]=32)and(a7[5,2]=36)and(a7[5,3]=34)and(a7[5,4]=33)and(a7[5,5]=4)and(a7[5,6]=31)and(a7[5,7]=35)and
          (a7[6,1]=7)and(a7[6,2]=12)and(a7[6,3]=9)and(a7[6,4]=8)and(a7[6,5]=10)and(a7[6,6]=0)and(a7[6,7]=11)and
          (a7[7,1]=38)and(a7[7,2]=42)and(a7[7,3]=40)and(a7[7,4]=39)and(a7[7,5]=41)and(a7[7,6]=37)and(a7[7,7]=5)then
          begin
            Timer1.Enabled:=False;
            Winner:= True;
            Application.MessageBox('�� ��������!',PChar(Caption),MB_ICONINFORMATION+MB_OK);
            CheckPlace(7);
            FDlgRes.ShowModal;
          end;
  end;
end;

procedure TFMain.ClickUp(Row: Byte);
var
  i, Temp: Byte;
begin
  case Image1.Height of
    120:
      begin
        Temp:=a7[Row,1];
        a7[Row,1]:=a7[Row,2];
        a7[Row,2]:=a7[Row,3];
        a7[Row,3]:=a7[Row,4];
        a7[Row,4]:=Temp;
        for i:=0 to 3 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[Row,i+1],bmp);
          Image1.Canvas.Draw((Row-1)*30,i*30,bmp);
        end;
        IfGameOver;
      end;
    150:
      begin
        Temp:=a7[Row,1];
        a7[Row,1]:=a7[Row,2];
        a7[Row,2]:=a7[Row,3];
        a7[Row,3]:=a7[Row,4];
        a7[Row,4]:=a7[Row,5];
        a7[Row,5]:=Temp;
        for i:=0 to 4 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[Row,i+1],bmp);
          Image1.Canvas.Draw((Row-1)*30,i*30,bmp);
        end;
        IfGameOver;
      end;
    180:
      begin
        Temp:=a7[Row,1];
        a7[Row,1]:=a7[Row,2];
        a7[Row,2]:=a7[Row,3];
        a7[Row,3]:=a7[Row,4];
        a7[Row,4]:=a7[Row,5];
        a7[Row,5]:=a7[Row,6];
        a7[Row,6]:=Temp;
        for i:=0 to 5 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[Row,i+1],bmp);
          Image1.Canvas.Draw((Row-1)*30,i*30,bmp);
        end;
         IfGameOver;
      end;
    210:
      begin
        Temp:=a7[Row,1];
        a7[Row,1]:=a7[Row,2];
        a7[Row,2]:=a7[Row,3];
        a7[Row,3]:=a7[Row,4];
        a7[Row,4]:=a7[Row,5];
        a7[Row,5]:=a7[Row,6];
        a7[Row,6]:=a7[Row,7];
        a7[Row,7]:=Temp;
        for i:=0 to 6 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[Row,i+1],bmp);
          Image1.Canvas.Draw((Row-1)*30,i*30,bmp);
        end;
        IfGameOver;
      end;
  end;
end;

procedure TFMain.ClickLeft(Line: Byte);
var
  i, Temp: Byte;
begin
  case Image1.Height of
    120:
      begin
        Temp:=a7[1,Line];
        a7[1,Line]:=a7[2,Line];
        a7[2,Line]:=a7[3,Line];
        a7[3,Line]:=a7[4,Line];
        a7[4,Line]:=Temp;
        for i:=0 to 3 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[i+1,Line],bmp);
          Image1.Canvas.Draw(i*30,(Line-1)*30,bmp);
        end;
        IfGameOver;
      end;
    150:
      begin
        Temp:=a7[1,Line];
        a7[1,Line]:=a7[2,Line];
        a7[2,Line]:=a7[3,Line];
        a7[3,Line]:=a7[4,Line];
        a7[4,Line]:=a7[5,Line];
        a7[5,Line]:=Temp;
        for i:=0 to 4 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[i+1,Line],bmp);
          Image1.Canvas.Draw(i*30,(Line-1)*30,bmp);
        end;
        IfGameOver;
      end;
    180:
      begin
        Temp:=a7[1,Line];
        a7[1,Line]:=a7[2,Line];
        a7[2,Line]:=a7[3,Line];
        a7[3,Line]:=a7[4,Line];
        a7[4,Line]:=a7[5,Line];
        a7[5,Line]:=a7[6,Line];
        a7[6,Line]:=Temp;
        for i:=0 to 5 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[i+1,Line],bmp);
          Image1.Canvas.Draw(i*30,(Line-1)*30,bmp);
        end;
        IfGameOver;
      end;
    210:
      begin
        Temp:=a7[1,Line];
        a7[1,Line]:=a7[2,Line];
        a7[2,Line]:=a7[3,Line];
        a7[3,Line]:=a7[4,Line];
        a7[4,Line]:=a7[5,Line];
        a7[5,Line]:=a7[6,Line];
        a7[6,Line]:=a7[7,Line];
        a7[7,Line]:=Temp;
        for i:=0 to 6 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[i+1,Line],bmp);
          Image1.Canvas.Draw(i*30,(Line-1)*30,bmp);
        end;
        IfGameOver;
      end;
  end;
end;

procedure TFMain.ClickDown(Row: Byte);
var
  i, Temp: Byte;
begin
  case Image1.Height of
    120:
      begin
        Temp:=a7[Row,4];
        a7[Row,4]:=a7[Row,3];
        a7[Row,3]:=a7[Row,2];
        a7[Row,2]:=a7[Row,1];
        a7[Row,1]:=Temp;
        for i:=0 to 3 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[Row,i+1],bmp);
          Image1.Canvas.Draw((Row-1)*30,i*30,bmp);
        end;
        IfGameOver;
      end;
    150:
      begin
        Temp:=a7[Row,5];
        a7[Row,5]:=a7[Row,4];
        a7[Row,4]:=a7[Row,3];
        a7[Row,3]:=a7[Row,2];
        a7[Row,2]:=a7[Row,1];
        a7[Row,1]:=Temp;
        for i:=0 to 4 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[Row,i+1],bmp);
          Image1.Canvas.Draw((Row-1)*30,i*30,bmp);
        end;
        IfGameOver;
      end;
    180:
      begin
        Temp:=a7[Row,6];
        a7[Row,6]:=a7[Row,5];
        a7[Row,5]:=a7[Row,4];
        a7[Row,4]:=a7[Row,3];
        a7[Row,3]:=a7[Row,2];
        a7[Row,2]:=a7[Row,1];
        a7[Row,1]:=Temp;
        for i:=0 to 5 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[Row,i+1],bmp);
          Image1.Canvas.Draw((Row-1)*30,i*30,bmp);
        end;
        IfGameOver;
      end;
    210:
      begin
        Temp:=a7[Row,7];
        a7[Row,7]:=a7[Row,6];
        a7[Row,6]:=a7[Row,5];
        a7[Row,5]:=a7[Row,4];
        a7[Row,4]:=a7[Row,3];
        a7[Row,3]:=a7[Row,2];
        a7[Row,2]:=a7[Row,1];
        a7[Row,1]:=Temp;
        for i:=0 to 6 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[Row,i+1],bmp);
          Image1.Canvas.Draw((Row-1)*30,i*30,bmp);
        end;
        IfGameOver;
      end;
  end;
end;

procedure TFMain.ClickRight (Line: Byte);
var
  i, Temp: Byte;
begin
  case Image1.Height of
    120:
      begin
        Temp:=a7[4,Line];
        a7[4,Line]:=a7[3,Line];
        a7[3,Line]:=a7[2,Line];
        a7[2,Line]:=a7[1,Line];
        a7[1,Line]:=Temp;
        for i:=0 to 3 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[i+1,Line],bmp);
          Image1.Canvas.Draw(i*30,(Line-1)*30,bmp);
        end;
        IfGameOver;
      end;
    150:
      begin
        Temp:=a7[5,Line];
        a7[5,Line]:=a7[4,Line];
        a7[4,Line]:=a7[3,Line];
        a7[3,Line]:=a7[2,Line];
        a7[2,Line]:=a7[1,Line];
        a7[1,Line]:=Temp;
        for i:=0 to 4 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[i+1,Line],bmp);
          Image1.Canvas.Draw(i*30,(Line-1)*30,bmp);
        end;
        IfGameOver;
      end;
    180:
      begin
        Temp:=a7[6,Line];
        a7[6,Line]:=a7[5,Line];
        a7[5,Line]:=a7[4,Line];
        a7[4,Line]:=a7[3,Line];
        a7[3,Line]:=a7[2,Line];
        a7[2,Line]:=a7[1,Line];
        a7[1,Line]:=Temp;
        for i:=0 to 5 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[i+1,Line],bmp);
          Image1.Canvas.Draw(i*30,(Line-1)*30,bmp);
        end;
        IfGameOver;
      end;
    210:
      begin
        Temp:=a7[7,Line];
        a7[7,Line]:=a7[6,Line];
        a7[6,Line]:=a7[5,Line];
        a7[5,Line]:=a7[4,Line];
        a7[4,Line]:=a7[3,Line];
        a7[3,Line]:=a7[2,Line];
        a7[2,Line]:=a7[1,Line];
        a7[1,Line]:=Temp;
        for i:=0 to 6 do
        begin
          bmp:=TBitmap.Create;
          ImageList1.GetBitmap(a7[i+1,Line],bmp);
          Image1.Canvas.Draw(i*30,(Line-1)*30,bmp);
        end;
        IfGameOver;
      end;
  end;
end;

procedure TFMain.SpeedButton01_1Click(Sender: TObject);
begin
  if not Winner then
    ClickUp(StrToInt((Sender as TSpeedButton).name[15]));
end;

procedure TFMain.SpeedButton12_1Click(Sender: TObject);
begin
  if not Winner then
    ClickLeft(StrToInt((Sender as TSpeedButton).name[15]));
end;

procedure TFMain.SpeedButton09_1Click(Sender: TObject);
begin
  if not Winner then
    ClickDown(StrToInt((Sender as TSpeedButton).name[15]));
end;

procedure TFMain.SpeedButton04_1Click(Sender: TObject);
begin
  if not Winner then
    ClickRight(StrToInt((Sender as TSpeedButton).name[15]));
end;

procedure TFMain.Timer1Timer(Sender: TObject);
begin
  label_sec.caption:=inttostr(StrToInt(label_sec.Caption)+1);
  if length(label_sec.caption) = 1 then
    label_sec.caption:='0' + label_sec.caption;
  if label_sec.caption='60' then
  begin
    label_sec.caption:='00';
    label_min.Caption:=inttostr(StrToInt(label_min.Caption)+1);
    if length(label_min.caption) = 1 then
      label_min.caption:='0' + label_min.caption;
  end;
end;

procedure TFMain.CheckPlace(GameType: Byte);
begin
  case GameType of
    4:
      if (StrToInt(label_min.Caption)<=StrToInt(copy(FDlgRes.Res.FP4.Time,1,2)))and(StrToInt(label_sec.Caption)<=StrToInt(copy(FDlgRes.Res.FP4.Time,4,2))) then
      begin
        FDlgName.ShowModal;
        FDlgRes.Res.FP4.Name:=FDlgName.Edit1.Text;
        FDlgRes.Res.FP4.Time:=Label_min.Caption+Label_sep.Caption+Label_sec.Caption;
      end;
    5:
      if (StrToInt(label_min.Caption)<=StrToInt(copy(FDlgRes.Res.FP5.Time,1,2)))and(StrToInt(label_sec.Caption)<=StrToInt(copy(FDlgRes.Res.FP5.Time,4,2))) then
      begin
        FDlgName.ShowModal;
        FDlgRes.Res.FP5.Name:=FDlgName.Edit1.Text;
        FDlgRes.Res.FP5.Time:=Label_min.Caption+Label_sep.Caption+Label_sec.Caption;
      end;
    6:
      if (StrToInt(label_min.Caption)<=StrToInt(copy(FDlgRes.Res.FP6.Time,1,2)))and(StrToInt(label_sec.Caption)<=StrToInt(copy(FDlgRes.Res.FP6.Time,4,2))) then
      begin
        FDlgName.ShowModal;
        FDlgRes.Res.FP6.Name:=FDlgName.Edit1.Text;
        FDlgRes.Res.FP6.Time:=Label_min.Caption+Label_sep.Caption+Label_sec.Caption;
      end;
    7:
      if (StrToInt(label_min.Caption)<=StrToInt(copy(FDlgRes.Res.FP7.Time,1,2)))and(StrToInt(label_sec.Caption)<=StrToInt(copy(FDlgRes.Res.FP7.Time,4,2))) {Count<FDlgRes.Res.FP7.Count} then
      begin
        FDlgName.ShowModal;
        FDlgRes.Res.FP7.Name:=FDlgName.Edit1.Text;
        FDlgRes.Res.FP7.Time:=Label_min.Caption+Label_sep.Caption+Label_sec.Caption;
      end;
  end;
end;

procedure TFMain.N6Click(Sender: TObject);
begin
  FDlgRes.ShowModal;
end;

procedure TFMain.N44Click(Sender: TObject);
begin
  N44.Checked:=True;
  N55.Checked:=False;
  N66.Checked:=False;
  N77.Checked:=False;
  DrawCell(4);
end;

procedure TFMain.N55Click(Sender: TObject);
begin
  N44.Checked:=False;
  N55.Checked:=True;
  N66.Checked:=False;
  N77.Checked:=False;
  DrawCell(5);
end;

procedure TFMain.N66Click(Sender: TObject);
begin
  N44.Checked:=False;
  N55.Checked:=False;
  N66.Checked:=True;
  N77.Checked:=False;
  DrawCell(6);
end;

procedure TFMain.N77Click(Sender: TObject);
begin
  N44.Checked:=False;
  N55.Checked:=False;
  N66.Checked:=False;
  N77.Checked:=True;
  DrawCell(7);
end;

procedure TFMain.N3Click(Sender: TObject);
begin
  FDlgAbout.ShowModal;
end;

procedure TFMain.N4Click(Sender: TObject);
begin
  Close;
end;

procedure TFMain.FormCreate(Sender: TObject);
begin
  N77.Click;
end;

procedure TFMain.N7Click(Sender: TObject);
begin
  FDlgHelp.ShowModal;
end;

procedure TFMain.N12Click(Sender: TObject);
begin
  if FileExists(ExtractFilePath(ParamStr(0))+'Rainbow1D.exe')then
  begin
    ShellExecute(0,'',PChar(ExtractFilePath(ParamStr(0))+'Rainbow1D.exe'),'','',1);
    Close;
  end
  else
    Application.MessageBox(PChar('���� �� ������'#10#13+ExtractFilePath(ParamStr(0))+'Rainbow1D.exe'),PChar(Caption),mb_ok+MB_ICONERROR);
end;

end.
