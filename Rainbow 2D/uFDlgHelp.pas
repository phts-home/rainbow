unit uFDlgHelp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TFDlgHelp = class(TForm)
    Image1: TImage;
    Button1: TButton;
    Bevel1: TBevel;
    Memo1: TMemo;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDlgHelp: TFDlgHelp;

implementation

uses uFMain;

{$R *.dfm}

procedure TFDlgHelp.FormShow(Sender: TObject);
begin
  FMain.Timer1.Enabled:=False;
  Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
  Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
end;

procedure TFDlgHelp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FMain.Timer1.Enabled:=not FMain.Winner;
end;

end.
