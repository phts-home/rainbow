object FDlgRes: TFDlgRes
  Left = 420
  Top = 278
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1055#1086#1073#1077#1076#1080#1090#1077#1083#1080
  ClientHeight = 162
  ClientWidth = 274
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label8: TLabel
    Left = 95
    Top = 80
    Width = 135
    Height = 13
    Caption = #1053#1077#1080#1079#1074#1077#1089#1090#1085#1099#1081' 9999  (99:59)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 95
    Top = 60
    Width = 135
    Height = 13
    Caption = #1053#1077#1080#1079#1074#1077#1089#1090#1085#1099#1081' 9999  (99:59)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 95
    Top = 40
    Width = 135
    Height = 13
    Caption = #1053#1077#1080#1079#1074#1077#1089#1090#1085#1099#1081' 9999  (99:59)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 95
    Top = 20
    Width = 135
    Height = 13
    Caption = #1053#1077#1080#1079#1074#1077#1089#1090#1085#1099#1081' 9999  (99:59)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 35
    Top = 80
    Width = 23
    Height = 13
    Caption = '7 x 7'
  end
  object Label3: TLabel
    Left = 35
    Top = 60
    Width = 23
    Height = 13
    Caption = '6 x 6'
  end
  object Label2: TLabel
    Left = 35
    Top = 40
    Width = 23
    Height = 13
    Caption = '5 x 5'
  end
  object Label1: TLabel
    Left = 35
    Top = 20
    Width = 23
    Height = 13
    Caption = '4 x 4'
  end
  object BtnClose: TButton
    Left = 180
    Top = 125
    Width = 75
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 2
    TabOrder = 0
  end
  object BtnReset: TButton
    Left = 15
    Top = 125
    Width = 136
    Height = 25
    Caption = #1057#1073#1088#1086#1089' '#1074#1089#1077#1093' '#1088#1077#1079#1091#1083#1100#1090#1072#1090#1086#1074
    TabOrder = 1
    OnClick = BtnResetClick
  end
end
