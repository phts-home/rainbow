program Rainbow;

uses
  Forms,
  uFMain in 'uFMain.pas' {FMain},
  uFDlgRes in 'uFDlgRes.pas' {FDlgRes},
  uFDlgName in 'uFDlgName.pas' {FDlgName},
  uFDlgAbout in 'uFDlgAbout.pas' {FDlgAbout},
  uFDlgHelp in 'uFDlgHelp.pas' {FDlgHelp};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := '������ 2D';
  Application.CreateForm(TFMain, FMain);
  Application.CreateForm(TFDlgRes, FDlgRes);
  Application.CreateForm(TFDlgName, FDlgName);
  Application.CreateForm(TFDlgAbout, FDlgAbout);
  Application.CreateForm(TFDlgHelp, FDlgHelp);
  Application.Run;
end.
