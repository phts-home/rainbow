unit uFDlgName;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls;

type
  TFDlgName = class(TForm)
    Edit1: TEdit;
    Label_n: TLabel;
    Button1: TButton;
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDlgName: TFDlgName;

implementation

uses uFMain;

{$R *.dfm}

procedure TFDlgName.FormShow(Sender: TObject);
begin
  Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
  Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
  Edit1.SetFocus;
end;

procedure TFDlgName.Button1Click(Sender: TObject);
begin
  if Edit1.Text <> '' then
    Close
  else
    Edit1.SetFocus;
end;

end.
