unit uFDlgRes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, ExtCtrls;

type
  Champ = Record
    FP4, FP5, FP6, FP7{, SP4, SP5, SP6, SP7}: Record
      Name: String[15];
      Time: String[5];
    end;
  end;

  TFDlgRes = class(TForm)
    BtnClose: TButton;
    BtnReset: TButton;
    Label8: TLabel;
    Label7: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure BtnResetClick(Sender: TObject);
  private
    { Private declarations }
  public
    FRes: File of Champ;
    Res: Champ;
    procedure ClearResults;
  end;

var
  FDlgRes: TFDlgRes;

implementation

uses uFMain;



{$R *.dfm}

procedure TFDlgRes.FormCreate(Sender: TObject);
begin
  ClearResults;
  AssignFile(FRes,ExtractFilePath(ParamStr(0))+'ResultsR2D.dat');
  try Reset(FRes);
  except
    Rewrite(FRes);
    Write(FRes,Res);
    CloseFile(FRes);
    Reset(FRes);
  end;
  while not eof(FRes) do
    Read(FRes,Res);
end;

procedure TFDlgRes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Rewrite(FDlgRes.FRes);
  Write(FDlgRes.FRes,FDlgRes.Res);
  FMain.Timer1.Enabled:=not FMain.Winner;
end;

procedure TFDlgRes.FormShow(Sender: TObject);
begin
  FMain.Timer1.Enabled:=False;
  label5.Caption:=Res.FP4.Time+'   '+Res.FP4.Name;
  label6.Caption:=Res.FP5.Time+'   '+Res.FP5.Name;
  label7.Caption:=Res.FP6.Time+'   '+Res.FP6.Name;
  label8.Caption:=Res.FP7.Time+'   '+Res.FP7.Name;
  Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
  Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
end;

procedure TFDlgRes.ClearResults;
begin
  Res.FP4.Name:='�����������';
  Res.FP5.Name:='�����������';
  Res.FP6.Name:='�����������';
  Res.FP7.Name:='�����������';
  Res.FP4.Time:='99:59';
  Res.FP5.Time:='99:59';
  Res.FP6.Time:='99:59';
  Res.FP7.Time:='99:59';
  label5.Caption:=Res.FP4.Time+'   '+Res.FP4.Name;
  label6.Caption:=Res.FP5.Time+'   '+Res.FP5.Name;
  label7.Caption:=Res.FP6.Time+'   '+Res.FP6.Name;
  label8.Caption:=Res.FP7.Time+'   '+Res.FP7.Name;
end;

procedure TFDlgRes.BtnResetClick(Sender: TObject);
begin
  if application.MessageBox('�� ������ �������� ��� ����������?',PChar(Caption),
    mb_YesNo+mb_iconQuestion) = IDYES then
    ClearResults;
end;

end.
